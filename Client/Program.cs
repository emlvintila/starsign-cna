﻿using System;
using System.Globalization;
using System.Threading.Tasks;
using Common;
using Grpc.Core;

namespace Client
{
  internal static class Program
  {
    private static async Task Main(string[] args)
    {
      Channel channel = new Channel("localhost", 48651, ChannelCredentials.Insecure);
      StarSignService.StarSignServiceClient client = new StarSignService.StarSignServiceClient(channel);

      DateTime dateTime;
      while (true)
      {
        Console.WriteLine("Input a date in the MM/dd/yyyy format: ");
        string input = Console.ReadLine();
        if (DateTime.TryParseExact(input, new[] {"M/d/yyyy", "M/dd/yyyy", "MM/d/yyyy", "MM/dd/yyyy"}, null, DateTimeStyles.None, out dateTime))
          break;
      }

      StarSignResponse response = await client.GetStarSignAsync(new StarSignRequest {Timestamp = dateTime.Ticks});
      Console.WriteLine(response.StarSign);

      await channel.ShutdownAsync();

      Console.WriteLine("Press any key to exit...");
      Console.ReadKey(true);
    }
  }
}
