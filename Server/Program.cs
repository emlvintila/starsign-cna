﻿using System;
using System.Threading.Tasks;
using Common;
using Grpc.Core;

namespace Server
{
  internal static class Program
  {
    private static async Task Main(string[] args)
    {
      Grpc.Core.Server server = new Grpc.Core.Server
      {
        Services = {StarSignService.BindService(new StarSignServiceImpl())}, Ports = {new ServerPort("localhost", 48651, ServerCredentials.Insecure)}
      };
      server.Start();

      Console.WriteLine("Press any key to exit...");
      Console.ReadKey(true);

      await server.ShutdownAsync();
    }
  }
}
