﻿using System;
using System.Collections.Immutable;
using System.Threading.Tasks;
using Common;
using Grpc.Core;

namespace Server
{
  public class StarSignServiceImpl : StarSignService.StarSignServiceBase
  {
    private static StarSign GetStarSignForDate(long timestamp)
    {
      DateTime dt = new DateTime(timestamp);

      return StarSignInterval.GetForMonth(dt.Month).GetForDayOfMonth(dt.Day);
    }

    public override Task<StarSignResponse> GetStarSign(StarSignRequest request, ServerCallContext context) =>
      Task.FromResult(new StarSignResponse {StarSign = GetStarSignForDate(request.Timestamp)});

    private class StarSignInterval : IEquatable<StarSignInterval>, IComparable<StarSignInterval>
    {
      private static readonly IImmutableSet<StarSignInterval> Months = new[]
      {
        new StarSignInterval(1, 20, StarSign.Capricorn, StarSign.Aquarius),
        new StarSignInterval(2, 19, StarSign.Aquarius, StarSign.Pisces),
        new StarSignInterval(3, 21, StarSign.Pisces, StarSign.Aries),
        new StarSignInterval(4, 20, StarSign.Aries, StarSign.Taurus),
        new StarSignInterval(5, 21, StarSign.Taurus, StarSign.Gemini),
        new StarSignInterval(6, 21, StarSign.Gemini, StarSign.Cancer),
        new StarSignInterval(7, 23, StarSign.Cancer, StarSign.Leo),
        new StarSignInterval(8, 23, StarSign.Leo, StarSign.Virgo),
        new StarSignInterval(9, 23, StarSign.Virgo, StarSign.Libra),
        new StarSignInterval(10, 23, StarSign.Libra, StarSign.Scorpio),
        new StarSignInterval(11, 22, StarSign.Scorpio, StarSign.Sagittarius),
        new StarSignInterval(12, 22, StarSign.Capricorn, StarSign.Aquarius)
      }.ToImmutableSortedSet();

      private readonly int day;
      private readonly StarSign left;
      private readonly int month;
      private readonly StarSign right;

      private StarSignInterval(int month, int day, StarSign left, StarSign right)
      {
        this.month = month;
        this.day = day;
        this.left = left;
        this.right = right;
      }

      public int CompareTo(StarSignInterval other) => month.CompareTo(other.month);

      public bool Equals(StarSignInterval other) => other != null && month.Equals(other.month);

      public StarSign GetForDayOfMonth(int day) => day < this.day ? left : right;

      public override int GetHashCode() => month.GetHashCode();

      public static StarSignInterval GetForMonth(int month) =>
        Months.TryGetValue(GetForComparisonForMonth(month), out StarSignInterval interval)
          ? interval
          : throw new ArgumentException(nameof(month));

      private static StarSignInterval GetForComparisonForMonth(int month) => new StarSignInterval(month, -1, StarSign.Invalid, StarSign.Invalid);
    }
  }
}
